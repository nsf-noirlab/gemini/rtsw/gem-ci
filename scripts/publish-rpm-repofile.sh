#!/bin/bash -e
#aliases for switch on/off command printout to console
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

source /etc/profile
echo TDCT: $TDCT

trace_on

echo "BRANCH is set to $BRANCH"

trace_on
# make sure the ssh key of hbfswgrepo-lv1 is in .ssh/known_hosts
ssh -o StrictHostKeyChecking=no -p $GEMCI_RSYNC_PORT "$GEMCI_RSYNC_USERNAME@${GEMCI_RSYNC_BASEURL%%:*}" \
        "echo importing public key from ${GEMCI_RSYNC_BASEURL%%:*}"

    # unlink .tito/releasers.conf && mv releasers.conf  .tito/releasers.conf

    mv gem-rtsw.repo /etc/yum.repos.d/gem-rtsw.repo 

    rsync -rlvz --delete /etc/yum.repos.d/gem-rtsw.repo ${GEMCI_RSYNC_USERNAME}@${GEMCI_RSYNC_BASEURL}/gem-rtsw-${MATURITY}/${TYPE}/${BRANCH}/centos-8/x86_64/

    GEM_REPO_URL=${GEMCI_RPMREPO_BASEURL}/gem-rtsw-${MATURITY}/${TYPE}/${BRANCH}/centos-8/x86_64/gem-rtsw.repo

    # make the vars usable for the gitlab-ci 'environment'
    echo "GEM_REPO_URL=$GEM_REPO_URL" >> build.env
    echo "TYPE=$TYPE" >> build.env
    echo "MATURITY=$MATURITY" >> build.env
    echo "BRANCH=$BRANCH" >> build.env


trace_off

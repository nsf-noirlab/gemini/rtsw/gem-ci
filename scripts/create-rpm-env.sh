#!/bin/bash -e
#aliases for switch on/off command printout to console
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

echo "rules-recognized TYPE: $TYPE"

# create branch-specific RPM repo directory and initialize it from 'source' repo, 
# if testing repo is existing and unstable repo is empty
echo creating $RPM_REPO directory on ${GEMCI_RSYNC_BASEURL%%:*}

trace_on

#strip off MATURITY from CI_COMMIT_BRANCH
BRANCH=${CI_COMMIT_BRANCH#*/}

# CALLER_BRANCH is set if an extra repository
# needs to be created
if [ "$CALLER_BRANCH" != "" ]; then
    BRANCH=$CALLER_BRANCH
fi

if [ "$CALLER_MATURITY" != "" ]; then
    MATURITY=$CALLER_MATURITY
fi

RPMREPO_PATH="${GEMCI_RSYNC_BASEURL##*:}/gem-rtsw-$MATURITY/$TYPE/$BRANCH/centos-8/x86_64"
exit_code=0

#ssh -o StrictHostKeyChecking=no "$GEMCI_RSYNC_USERNAME@${GEMCI_RSYNC_BASEURL%%:*}" \
#    "[ ! -d $RPMREPO_PATH ] && mkdir -p $RPMREPO_PATH && cd $RPMREPO_PATH && createrepo_c ." \
#    || echo RPM repository directory $RPMREPO_PATH already existing, ommiting to initialize it
ssh -o StrictHostKeyChecking=no -p $GEMCI_RSYNC_PORT "$GEMCI_RSYNC_USERNAME@${GEMCI_RSYNC_BASEURL%%:*}" \
    "[ ! -d $RPMREPO_PATH ] && mkdir -p $RPMREPO_PATH && touch $RPMREPO_PATH/gem-rtsw.repo" || exit_code=$?

# initialize repo if the directory was just created above
# commenting out, because the step above exits with error code 1
#if [ $exit_code -lt 2 ]; then
mkdir tmp
createrepo tmp/
rsync -rlvz --delete tmp/ ${GEMCI_RSYNC_USERNAME}@${GEMCI_RSYNC_BASEURL}/gem-rtsw-${MATURITY}/${TYPE}/${BRANCH}/centos-8/x86_64
#fi

### take out for now. I think it's not neccesary, but there probably was a reason to add this before... 
#ssh -o StrictHostKeyChecking=no "$GEMCI_RSYNC_USERNAME@${GEMCI_RSYNC_BASEURL%%:*}" \
#    "cd $RPMREPO_PATH && createrepo_c ." \
#    || echo Failure to initialize $RPMREPO_PATH
trace_off


#!/bin/sh -e

set -x

echo "CI_PROJECT_NAME is set to $CI_PROJECT_NAME"
echo "TYPE is set to $TYPE"
echo "CALLER_BRANCH is set to $CALLER_BRANCH"
echo "CALLER_MATURITY is set to $CALLER_MATURITY"
echo "CALLER_SHORT_SHA is set to $CALLER_SHORT_SHA"
rm -rf callers
BRANCH=$CI_COMMIT_BRANCH
  
if [ "$CALLER_MATURITY" != "" ]; then
    MATURITY=$CALLER_MATURITY
fi

if [ "$CALLER_BRANCH" != "" ]; then
    BRANCH=$MATURITY/$CALLER_BRANCH
fi

SHORT_SHA=$CI_COMMIT_SHORT_SHA
if [ "$CALLER_SHORT_SHA" != "" ]; then
    SHORT_SHA=$CALLER_SHORT_SHA
fi

PROJECT_NAME=$CI_PROJECT_NAME
if [ "$CALLER_PROJECT" != "" ]; then
    PROJECT_NAME=$CALLER_PROJECT
fi

# copy Gemini-specific certs into build context for accessing rocky mirrors
mkdir -p /etc/pki/entitlement
mkdir -p /etc/rhsm
cp -r /etc/pki/entitlement .
cp -r /etc/rhsm .

# login
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
# use deprecated builder, because DNS does not work correctly with buildx
export DOCKER_BUILDKIT=0
# docker buildx create --use --driver docker-container --driver-opt network=host
if [ "$BUILD_BASE_IMAGE" == "true" ]; then
    # build container image for the tag 'latest'
    docker build . -t $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH} | tr '[:upper:]' '[:lower:]') --no-cache -f Containerfile.base_image --network=gemci_buildenv-net --build-arg GEMCI_TAGS=$GEMCI_TAGS
    # sleep to let the local registry some time to settle
    sleep 10
    docker push $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH} | tr '[:upper:]' '[:lower:]')
    # build base container image for tag base_image
    docker build . -t $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH}:base_image | tr '[:upper:]' '[:lower:]') -f Containerfile.base_image --network=gemci_buildenv-net --build-arg GEMCI_TAGS=$GEMCI_TAGS
    # sleep to let the local registry some time to settle
    sleep 10
    docker push $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH}:base_image | tr '[:upper:]' '[:lower:]')
else
    # build container image for the tag 'latest'
    docker build . -t $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH} | tr '[:upper:]' '[:lower:]') --no-cache -f Containerfile --network=gemci_buildenv-net --build-arg GEMCI_TAGS=$GEMCI_TAGS

    # sleep to let the local registry some time to settle
    sleep 10
    docker push $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH} | tr '[:upper:]' '[:lower:]')
    # and with tag $CI_COMMIT_SHORT_SHA and the $CALLER_ID of the calling project.
    # This way the container is linked to the revision of the project 
    # which triggered the build of the container
    docker build . -t $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH}:$PROJECT_NAME-${SHORT_SHA} | tr '[:upper:]' '[:lower:]') -f Containerfile --network=gemci_buildenv-net --build-arg GEMCI_TAGS=$GEMCI_TAGS

    sleep 10
    docker push $(echo $CI_REGISTRY_IMAGE/${TYPE}_${BRANCH}:$PROJECT_NAME-${SHORT_SHA} | tr '[:upper:]' '[:lower:]')
fi
# create artifacts to let caller job know that the triggered pipeline finished
mkdir -p callers/$CALLER_ID
touch callers/$CALLER_ID/dummy.txt


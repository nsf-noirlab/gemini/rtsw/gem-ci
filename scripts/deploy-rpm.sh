#!/bin/bash -e
#aliases for switch on/off command printout to console
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

source /etc/profile
echo TDCT: $TDCT

# default for 'app'
#PROJECT_NAME=$CI_PROJECT_NAME

trace_on

#strip off MATURITY from CI_COMMIT_BRANCH
BRANCH=${CI_COMMIT_BRANCH#*/}

#echo current container: $BASE_CONTAINER

ssh -o StrictHostKeyChecking=no -p $GEMCI_RSYNC_PORT "$GEMCI_RSYNC_USERNAME@${GEMCI_RSYNC_BASEURL%%:*}" \
    "echo importing public key of ${GEMCI_RSYNC_BASEURL%%:*}"

unlink .tito/releasers.conf 

# create releasers.conf from template
sed -e "s#<PROJECTNAME>#$PROJECT_NAME#g" \
  -e "s#<BRANCH>#$BRANCH#g" \
  -e "s#<MATURITY>#$MATURITY#g" \
  -e "s#<GEMCI_RSYNC_BASEURL>#$GEMCI_RSYNC_BASEURL#g" \
  gem-ci/templates/tito/$TYPE/releasers.conf > .tito/releasers.conf

RSYNC_HOST=${GEMCI_RSYNC_BASEURL%%:*}

# set rsync ssh port using the ssh config file
# otherwise it would not work with tito
cat << EOF > ~/.ssh/config
Host ${RSYNC_HOST}
    Port ${GEMCI_RSYNC_PORT}
EOF

if [ "${CI_PROJECT_NAME}" == "rtems" ]; then
    ./rtems-setup.sh
    # do what tito normally does, but which is impossible here because the specfile is created on the fly
    rsync -rlvz  ${GEMCI_RSYNC_USERNAME}@${GEMCI_RSYNC_BASEURL}/gem-rtsw-${MATURITY}/epics-base/${BRANCH}/centos-8/x86_64/* tmp/
    mv  rtems-deployment/out/buildroot/RPMS/x86_64/*.rpm tmp/
    createrepo tmp/
    rsync -rlvz --delete tmp/* ${GEMCI_RSYNC_USERNAME}@${GEMCI_RSYNC_BASEURL}/gem-rtsw-${MATURITY}/epics-base/${BRANCH}/centos-8/x86_64/
#    ssh  ${GEMCI_RSYNC_USERNAME}@${GEMCI_RPMREPO_HOSTNAME} "cd ${GEMCI_RSYNC_BASEURL#*:}/gem-rtsw-${MATURITY}/epics-base/${BRANCH}/centos-8/x86_64/; createrepo ."
else
    # install package dependencies prior to build
    dnf builddep -y $(ls *.spec)

    RSYNC_USERNAME=${GEMCI_RSYNC_USERNAME} tito release gem-rtsw-$TYPE -o tmp  
    rm -rf tmp
fi

# sync repos to SBF
if [ "$RSYNC_HOST" == "hbfswgrepo-lv1.hi.gemini.edu" ]; then
    ssh ${GEMCI_RSYNC_USERNAME}@${RSYNC_HOST} "rsync -av --delete /rtsw/packages/ sbfswgrepo-lv1.cl.gemini.edu:/rtsw/packages/"
fi
trace_off

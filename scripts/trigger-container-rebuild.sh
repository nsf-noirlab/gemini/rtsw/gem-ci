#!/bin/bash -e

#aliases for switch on/off command printout to console
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

trace_off
echo "triggering https://gitlab.com/api/v4/projects/$GEM_CI_PROJECT_HTML/trigger/pipeline with
            ref=$GEM_CI_BRANCH 
            [TYPE]=$TYPE
            [CALLER_MATURITY]=$MATURITY
            [CALLER_BRANCH]=$TARGET_BRANCH
            [CALLER_SHORT_SHA]=$CI_COMMIT_SHORT_SHA
            [BASE_CONTAINER]=$NEW_BASE_CONTAINER
            [BUILD_BASE_IMAGE]=false
            [SSH_PRIVATE_BASE64]=[not shown]
            [GEMCI_TAGS]=$GEMCI_TAGS
            [GEMCI_RSYNC_BASEURL]=$GEMCI_RSYNC_BASEURL
            [GEMCI_RSYNC_PORT]=$GEMCI_RSYNC_PORT
            [GEMCI_RPMREPO_HOSTNAME]=$GEMCI_RPMREPO_HOSTNAME
            [GEMCI_RPMREPO_BASEURL]=$GEMCI_RPMREPO_BASEURL
            [GEMCI_RSYNC_USERNAME]=$GEMCI_RSYNC_USERNAME
            [CALLER_PROJECT]=$CI_PROJECT_NAME
            [CALLER_ID]=$CI_PIPELINE_ID/$CI_JOB_ID"
curl --request POST --form token=$CI_JOB_TOKEN \
        --form ref=$GEM_CI_BRANCH \
        --form "variables[TYPE]=$TYPE" \
        --form "variables[CALLER_MATURITY]=$MATURITY" \
        --form "variables[CALLER_BRANCH]=$TARGET_BRANCH" \
        --form "variables[CALLER_SHORT_SHA]=$CI_COMMIT_SHORT_SHA" \
        --form "variables[BASE_CONTAINER]=$NEW_BASE_CONTAINER" \
        --form "variables[BUILD_BASE_IMAGE]=false" \
        --form "variables[SSH_PRIVATE_BASE64]=$SSH_PRIVATE_BASE64" \
        --form "variables[GEMCI_TAGS]=$GEMCI_TAGS" \
        --form "variables[GEMCI_RSYNC_BASEURL]=$GEMCI_RSYNC_BASEURL" \
        --form "variables[GEMCI_RSYNC_PORT]=$GEMCI_RSYNC_PORT" \
        --form "variables[GEMCI_RPMREPO_HOSTNAME]=$GEMCI_RPMREPO_HOSTNAME" \
        --form "variables[GEMCI_RPMREPO_BASEURL]=$GEMCI_RPMREPO_BASEURL" \
        --form "variables[GEMCI_RSYNC_USERNAME]=$GEMCI_RSYNC_USERNAME" \
        --form "variables[CALLER_PROJECT]=$CI_PROJECT_NAME" \
        --form "variables[CALLER_ID]=$CI_PIPELINE_ID/$CI_JOB_ID" \
        "https://gitlab.com/api/v4/projects/$GEM_CI_PROJECT_HTML/trigger/pipeline"

trace_on
echo "Waiting on $GEM_CI_PROJECT_HTML artifacts..."
ARTIFACT_URL="https://gitlab.com/api/v4/projects/$GEM_CI_PROJECT_HTML/jobs/artifacts/$GEM_CI_BRANCH/download?job=deploy-container"
trace_off
sleep 15
while :; do
    echo -n "."
    curl --silent --location -o artifacts.zip  --header "JOB-TOKEN: $CI_JOB_TOKEN" $ARTIFACT_URL
    unzip -l artifacts.zip | grep -q "$CI_PIPELINE_ID/$CI_JOB_ID/dummy.txt" && break
    sleep 15
done; echo

trace_off

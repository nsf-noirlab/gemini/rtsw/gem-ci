#!/bin/bash -e
#aliases for switch on/off command printout to console
shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'

trace_on

echo current branch: $CI_COMMIT_BRANCH

## extract BASE_CONTAINER's branch and type
# strip off registry name and namespace up to gem-ci
TMP=${BASE_CONTAINER##registry.gitlab.com/nsf-noirlab/gemini/rtsw/gem-ci/}
# cases like rockylinux:8 or so
if [ "$BASE_CONTAINER" == "$TMP" ]; then
    BASE_CONTAINER_TYPE=$TYPE
else
    # for the type, strip off all after the first '/' and then
    # after the first '_'
    TMP=${TMP%%/*}
    BASE_CONTAINER_TYPE=${TMP%%_*}
fi

GEM_CI_BRANCH=$(cat .gitlab-ci.yml |grep ref:|cut -d'"' -f2)
GEM_CI_PROJECT="nsf-noirlab/gemini/rtsw/gem-ci"
GEM_CI_PROJECT_HTML="nsf-noirlab%2Fgemini%2Frtsw%2Fgem-ci"

# the NEW_BASE_CONTAINER's maturity is extracted from branch name
# by deleting longest match of '/*' from end of string
MATURITY=${CI_COMMIT_BRANCH%%/*}
# the NEW_BASE_CONTAINER's 'branch' is extracted from branch name
# by deleting shortest match of '*/' from beginning of string
TARGET_BRANCH=${CI_COMMIT_BRANCH#*/}

# map maturity to correct internal ones (historical debt)
if [[ "$MATURITY" != "testing" && "$MATURITY" != "stable" ]]; then
    MATURITY="unstable"
fi

echo CONTAINER_PROJECT_BRANCH="$CI_COMMIT_BRANCH" >> prepare.env
echo GEM_CI_PROJECT="$GEM_CI_PROJECT" >> prepare.env
echo GEM_CI_PROJECT_HTML="$GEM_CI_PROJECT_HTML" >> prepare.env
echo GEM_CI_BRANCH="$GEM_CI_BRANCH" >> prepare.env
echo MATURITY="$MATURITY" >> prepare.env
echo TYPE="$TYPE" >> prepare.env
echo TARGET_BRANCH="$TARGET_BRANCH" >> prepare.env

NEW_BASE_CONTAINER="registry.gitlab.com/nsf-noirlab/gemini/rtsw/gem-ci/${TYPE}_${MATURITY}/${TARGET_BRANCH}:base_image"
# make it lowercase
NEW_BASE_CONTAINER=$(echo $NEW_BASE_CONTAINER | tr '[:upper:]' '[:lower:]')

echo NEW_BASE_CONTAINER=$NEW_BASE_CONTAINER >> prepare.env

# login
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
# check if container image exists on registry
# if not, trigger a multi-project pipeline to create it
# and wait for dummy artifacts to be generated so the
# local pipeline can continue.
result=$(docker manifest inspect $NEW_BASE_CONTAINER) || echo manifest not found, hence container not existing. Using container specified by BASE_CONTAINER 
# The digest of the BASE_CONTAINER that was used to build the base image of this layer
BC_DIGEST_LABELED=$(skopeo inspect docker://$NEW_BASE_CONTAINER | jq -r '.Labels["edu.gemini.rtsw.basecontainer.digest"]')
BC_DIGEST=$(skopeo inspect docker://$BASE_CONTAINER | jq -r '.Digest')
if [ "$BC_DIGEST" != "$BC_DIGEST_LABELED" ] && [ "$result" == "" ]; then
    # make use of setting in .gitlab-ci.yml (or API ot git push option or...)
    # echo NEW_BASE_CONTAINER=$BASE_CONTAINER >> prepare.env
    trace_off
    echo "triggering https://gitlab.com/api/v4/projects/$GEM_CI_PROJECT_HTML/trigger/pipeline with
            ref=$GEM_CI_BRANCH 
            [TYPE]=$TYPE
            [CALLER_MATURITY]=$MATURITY
            [CALLER_BRANCH]=$TARGET_BRANCH
            [CALLER_SHORT_SHA]=$CI_COMMIT_SHORT_SHA
            [BASE_CONTAINER]=$BASE_CONTAINER
            [BUILD_BASE_IMAGE]=true
            [SSH_PRIVATE_BASE64]=[not shown]
            [GEMCI_TAGS]=$GEMCI_TAGS
            [GEMCI_RSYNC_BASEURL]=$GEMCI_RSYNC_BASEURL
            [GEMCI_RSYNC_PORT]=$GEMCI_RSYNC_PORT
            [GEMCI_RPMREPO_HOSTNAME]=$GEMCI_RPMREPO_HOSTNAME
            [GEMCI_RPMREPO_BASEURL]=$GEMCI_RPMREPO_BASEURL
            [GEMCI_RSYNC_USERNAME]=$GEMCI_RSYNC_USERNAME
            [BC_DIGEST]=$BC_DIGEST
            [CALLER_PROJECT]=$CI_PROJECT_NAME
            [CALLER_ID]=$CI_PIPELINE_ID/$CI_JOB_ID"
    curl --request POST --form token=$CI_JOB_TOKEN \
            --form ref=$GEM_CI_BRANCH \
            --form "variables[TYPE]=$TYPE" \
            --form "variables[CALLER_MATURITY]=$MATURITY" \
            --form "variables[CALLER_BRANCH]=$TARGET_BRANCH" \
            --form "variables[CALLER_SHORT_SHA]=$CI_COMMIT_SHORT_SHA" \
            --form "variables[BASE_CONTAINER]=$BASE_CONTAINER" \
            --form "variables[BUILD_BASE_IMAGE]=true" \
            --form "variables[SSH_PRIVATE_BASE64]=$SSH_PRIVATE_BASE64" \
            --form "variables[GEMCI_TAGS]=$GEMCI_TAGS" \
            --form "variables[GEMCI_RSYNC_BASEURL]=$GEMCI_RSYNC_BASEURL" \
            --form "variables[GEMCI_RSYNC_PORT]=$GEMCI_RSYNC_PORT" \
            --form "variables[GEMCI_RPMREPO_HOSTNAME]=$GEMCI_RPMREPO_HOSTNAME" \
            --form "variables[GEMCI_RPMREPO_BASEURL]=$GEMCI_RPMREPO_BASEURL" \
            --form "variables[GEMCI_RSYNC_USERNAME]=$GEMCI_RSYNC_USERNAME" \
            --form "variables[BC_DIGEST]=$BC_DIGEST" \
            --form "variables[CALLER_PROJECT]=$CI_PROJECT_NAME" \
            --form "variables[CALLER_ID]=$CI_PIPELINE_ID/$CI_JOB_ID" \
            "https://gitlab.com/api/v4/projects/$GEM_CI_PROJECT_HTML/trigger/pipeline"
    trace_on

    echo "Waiting on $GEM_CI_PROJECT_HTML artifacts..."
    ARTIFACT_URL="https://gitlab.com/api/v4/projects/$GEM_CI_PROJECT_HTML/jobs/artifacts/$GEM_CI_BRANCH/download?job=deploy-container"
    trace_off
    sleep 15
    while :; do
        echo -n "."
        curl --silent --location -o artifacts.zip  --header "JOB-TOKEN: $CI_JOB_TOKEN" $ARTIFACT_URL
        unzip -l artifacts.zip | grep -q "$CI_PIPELINE_ID/$CI_JOB_ID/dummy.txt" && break
        sleep 15
    done; echo
fi
